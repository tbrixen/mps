/*
 ============================================================================
 Name        : text_formatter.c
 Author      : PHM
 Version     :
 Copyright   : Your copyright notice
 Description : Text Formatter C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "formatter.h"
int main(void) {
	puts("Running my formatting tool!"); 

	// Create formatter object
	formatter HTMLformatter; 
	formatter TEXTformatter; 
	
	// Invoke formatter constructor
	formatter_init(&HTMLformatter, HTML, "/Users/tobiasbrixen/Dropbox/AU/MPS/MPSProgrammer/formatter/abc");
	formatter_init(&TEXTformatter, TEXT, "/Users/tobiasbrixen/Dropbox/AU/MPS/MPSProgrammer/formatter/abc");

	// Use formatter methods
	formatter_header(&HTMLformatter, "head head HTML header");
	formatter_body(&HTMLformatter, "bod bod HTML body");
	formatter_header(&TEXTformatter, "head head TEXT header");
	formatter_body(&TEXTformatter, "bod bod TEXT body");

	// Invoke formatter destructor
	formatter_release(&HTMLformatter);
	formatter_release(&TEXTformatter);
	
	return EXIT_SUCCESS;
}
